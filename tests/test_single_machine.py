import pytest

from state_engine import Moore, Event, TransitionsImplementationError, EpsilonEvent


class EventOne(Event):
    pass


class EventTwo(Event):
    pass


class CustomMoore(Moore):
    def state_one(self, event: Event):
        return event

    def state_two(self, event: Event):
        return event

    def state_five(self, event: Event):
        return event


@pytest.mark.order(1)
class TestStateMachine:
    def setup(self):
        self.machine = CustomMoore("machine")
        self.machine.add_event(
            EventOne, {None: "state_one", "state_one": "state_two"}
        ).add_event(EventTwo, {"state_two": "state_one"})

    def test_create_machine(self):
        assert self.machine.events[EventOne] == {
            None: "state_one",
            "state_one": "state_two",
        }
        assert self.machine.events[EventTwo] == {"state_two": "state_one"}
        assert self.machine.name == "machine"

    def test_wrong_transitions_implementation(self):
        with pytest.raises(TransitionsImplementationError):
            self.machine.add_event(EventOne, {None: 1})

        with pytest.raises(TransitionsImplementationError):
            self.machine.add_event(EventOne, {3: None})

        with pytest.raises(TransitionsImplementationError):
            self.machine.add_event(EventOne, {"state_one": "state_four"})

        with pytest.raises(TransitionsImplementationError):
            self.machine.add_event(EventOne, {"state_three": "state_one"})

    def test_machines_equivalence(self):
        _machine = CustomMoore("machine")
        _machine.add_event(
            EventOne, {None: "state_one", "state_one": "state_two"}
        ).add_event(EventTwo, {"state_two": "state_one"})

        assert self.machine == _machine

    def test_execution(self):
        assert self.machine.state is None

        event = EventOne()

        assert self.machine.execute(event) == {(self.machine, "state_one"): event}
        assert self.machine.state == "state_one"

        assert self.machine.execute(event) == {(self.machine, "state_two"): event}
        assert self.machine.state is "state_two"

        assert self.machine.execute(event) is None
        assert self.machine.state is "state_two"

        event = EventTwo()

        assert self.machine.execute(event) == {(self.machine, "state_one"): event}
        assert self.machine.state is "state_one"

        self.machine.reset()
        assert self.machine.state is None

    def test_remove_event(self):
        self.machine.remove_event(EventOne)
        self.machine.execute(EventOne())
        assert EventOne not in self.machine.events

    def test_epsilon_transition(self):
        self.machine.add_event(EpsilonEvent, {"state_one": "state_five"})
        event = EventOne()
        assert list(self.machine.execute(event).keys()) == [
            (self.machine, "state_one"),
            (self.machine, "state_five"),
        ]
        assert self.machine.state == "state_five"

    def test_loop_execution(self):
        self.machine.add_event(EventTwo, {"state_two": "state_two"})
        event = EventOne()
        assert self.machine.execute(event) == {(self.machine, "state_one"): event}
        assert self.machine.execute(event) == {(self.machine, "state_two"): event}
        event = EventTwo()
        assert self.machine.execute(event) == {(self.machine, "state_two"): event}
