import pytest

from state_engine import Moore, Event, StateEvent, StateEventImplementationError


class EventOne(Event):
    pass


class EventTwo(Event):
    pass


class EventAny(Event):
    pass


class StateEventOne(StateEvent):

    states = {"and": [("machine_one", "state_one"), ("machine_two", "state_two")]}


class StateEventTwo(StateEvent):

    states = {
        "or": [
            {
                "and": [
                    ("machine_one", "state_one"),
                    {"not": ("machine_two", "state_two")},
                ]
            },
            ("machine_two", "state_two"),
        ],
    }


class CustomMoore(Moore):
    def state_all_two(self, event: Event):
        return event

    def state_any_in_one(self, event: Event):
        return event


class CustomMooreOne(Moore):
    def state_one(self, event: Event):
        return event

    def state_two(self, event: Event):
        return event


class CustomMooreTwo(Moore):
    def state_one(self, event: Event):
        return event

    def state_two(self, event: Event):
        return event


@pytest.mark.order(2)
class TestNestedStateMachine:
    def setup(self):
        self.nested_machine_one = CustomMooreOne("machine_one")
        self.nested_machine_one.add_event(
            EventOne, {None: "state_one", "state_one": "state_two"}
        ).add_event(EventTwo, {"state_two": "state_one"})

        self.nested_machine_two = CustomMooreTwo("machine_two")
        self.nested_machine_two.add_event(
            EventOne, {"state_one": "state_two"}
        ).add_event(EventTwo, {None: "state_two", "state_two": "state_one"})

        self.machine = CustomMoore("machine")
        self.machine.add_machine(self.nested_machine_one).add_machine(
            self.nested_machine_two
        )

    def test_create_nested_machine(self):
        assert self.nested_machine_one.parent == self.machine
        assert self.nested_machine_two.parent == self.machine
        assert self.nested_machine_one.name in self.machine
        assert self.nested_machine_two.name in self.machine

    def test_children_execution(self):
        event = EventOne()
        r = self.machine.execute(event)
        assert r == {(self.nested_machine_one, "state_one"): event}
        assert self.nested_machine_one.state == "state_one"
        assert self.nested_machine_two.state is None

        assert self.machine.execute(event) == {
            (self.nested_machine_one, "state_two"): event
        }
        assert self.nested_machine_one.state == "state_two"
        assert self.nested_machine_two.state is None

        event = EventTwo()
        assert self.machine.execute(event) == {
            (self.nested_machine_one, "state_one"): event,
            (self.nested_machine_two, "state_two"): event,
        }
        assert self.nested_machine_one.state == "state_one"
        assert self.nested_machine_two.state == "state_two"

        assert self.machine.execute(event) == {
            (self.nested_machine_two, "state_one"): event
        }
        assert self.nested_machine_one.state == "state_one"
        assert self.nested_machine_two.state == "state_one"

    def test_parent_execution(self):
        self.machine.add_event(StateEventOne, {None: "state_all_two"})
        self.machine.execute(EventOne())
        self.machine.execute(EventTwo())
        assert self.machine.state == "state_all_two"

    def test_loop_execution(self):
        self.machine.add_event(
            StateEventOne, {None: "state_all_two", "state_all_two": "state_all_two"}
        )
        self.machine.execute(EventOne())
        self.machine.execute(EventTwo())

    def test_loop_execution_with_loop_in_nested_machine(self):
        self.nested_machine_two.add_event(
            EventTwo, {None: "state_two", "state_two": "state_two"}
        )
        self.machine.add_event(
            StateEventOne, {None: "state_all_two", "state_all_two": "state_all_two"}
        )

        self.machine.execute(EventOne())
        assert self.nested_machine_one.state == "state_one"
        assert self.nested_machine_two.state is None

        self.machine.execute(EventTwo())
        assert self.nested_machine_one.state == "state_one"
        assert self.nested_machine_two.state == "state_two"

        self.machine.execute(EventTwo())
        assert self.nested_machine_one.state == "state_one"
        assert self.nested_machine_two.state == "state_two"

    def test_empty_state_machine_event(self):
        class EmptyStateEvent(StateEvent):
            pass

        with pytest.raises(StateEventImplementationError):
            self.machine.add_event(EmptyStateEvent, {None: "state_all_two"})

    def test_state_event_validation(self):
        self.machine.add_event(StateEventOne, {None: "state_all_two"}).add_event(
            StateEventTwo, {None: "state_all_two"}
        )

    def test_tuple_state_event(self):
        class TestStateEvent(StateEvent):
            states = ("machine_one", "state_one")

        self.machine.add_event(TestStateEvent, {None: "state_all_two"})
        self.machine.execute(EventOne())
        assert self.machine.state == "state_all_two"

    def test_or_state_event(self):
        class TestStateEvent(StateEvent):
            states = {
                "or": [("machine_one", "state_one"), ("machine_two", "state_one")]
            }

        self.machine.add_event(
            TestStateEvent,
            {None: "state_any_in_one", "state_any_in_one": "state_any_in_one"},
        )

        one = EventOne()
        two = EventTwo()

        r = self.machine.execute(one)
        assert isinstance(r[(self.machine, "state_any_in_one")], TestStateEvent)
        assert self.nested_machine_one.state == "state_one"
        assert self.nested_machine_two.state is None
        assert self.machine.state == "state_any_in_one"

        r = self.machine.execute(two)
        assert isinstance(r[(self.machine, "state_any_in_one")], TestStateEvent)
        assert {(self.nested_machine_two, "state_two"): two}.items() <= r.items()
        assert self.nested_machine_one.state == "state_one"
        assert self.nested_machine_two.state == "state_two"
        assert self.machine.state == "state_any_in_one"

        r = self.machine.execute(one)
        with pytest.raises(KeyError):
            isinstance(r[(self.machine, "state_any_in_one")], TestStateEvent)
        assert self.nested_machine_one.state == "state_two"
        assert self.nested_machine_two.state == "state_two"
        assert self.machine.state == "state_any_in_one"

        r = self.machine.execute(two)
        assert isinstance(r[(self.machine, "state_any_in_one")], TestStateEvent)
        assert self.nested_machine_one.state == "state_one"
        assert self.nested_machine_two.state == "state_one"
        assert self.machine.state == "state_any_in_one"

    def test_and_state_event(self):
        class TestStateEvent(StateEvent):
            states = {
                "and": [("machine_one", "state_one"), ("machine_two", "state_one")]
            }

        self.machine.add_event(
            TestStateEvent,
            {None: "state_any_in_one", "state_any_in_one": "state_any_in_one"},
        )

        one = EventOne()
        two = EventTwo()

        r = self.machine.execute(one)
        with pytest.raises(KeyError):
            isinstance(r[(self.machine, "state_any_in_one")], TestStateEvent)
        assert self.nested_machine_one.state == "state_one"
        assert self.nested_machine_two.state is None
        assert self.machine.state is None

        r = self.machine.execute(two)
        with pytest.raises(KeyError):
            isinstance(r[(self.machine, "state_any_in_one")], TestStateEvent)
        assert r == {(self.nested_machine_two, "state_two"): two}
        assert self.nested_machine_one.state == "state_one"
        assert self.nested_machine_two.state == "state_two"
        assert self.machine.state is None

        r = self.machine.execute(one)
        with pytest.raises(KeyError):
            isinstance(r[(self.machine, "state_any_in_one")], TestStateEvent)
        assert self.nested_machine_one.state == "state_two"
        assert self.nested_machine_two.state == "state_two"
        assert self.machine.state is None

        r = self.machine.execute(two)
        assert isinstance(r[(self.machine, "state_any_in_one")], TestStateEvent)
        assert self.nested_machine_one.state == "state_one"
        assert self.nested_machine_two.state == "state_one"
        assert self.machine.state == "state_any_in_one"

    def test_not_and_state_event(self):
        class TestStateEvent(StateEvent):
            states = {"not": {"and": [("machine_one", None), ("machine_two", None)]}}

        self.machine.add_event(
            TestStateEvent,
            {None: "state_any_in_one", "state_any_in_one": "state_any_in_one"},
        )

        one = EventOne()
        two = EventTwo()

        r = self.machine.execute(one)
        assert isinstance(r[(self.machine, "state_any_in_one")], TestStateEvent)
        assert self.nested_machine_one.state == "state_one"
        assert self.nested_machine_two.state is None
        assert self.machine.state == "state_any_in_one"

        r = self.machine.execute(two)
        assert isinstance(r[(self.machine, "state_any_in_one")], TestStateEvent)
        assert self.nested_machine_one.state == "state_one"
        assert self.nested_machine_two.state == "state_two"
        assert self.machine.state == "state_any_in_one"

        r = self.machine.execute(one)
        assert isinstance(r[(self.machine, "state_any_in_one")], TestStateEvent)
        assert self.nested_machine_one.state == "state_two"
        assert self.nested_machine_two.state == "state_two"
        assert self.machine.state == "state_any_in_one"

        r = self.machine.execute(two)
        assert isinstance(r[(self.machine, "state_any_in_one")], TestStateEvent)
        assert self.nested_machine_one.state == "state_one"
        assert self.nested_machine_two.state == "state_one"
        assert self.machine.state == "state_any_in_one"

    def test_not_or_state_event(self):
        class TestStateEvent(StateEvent):
            states = {"not": {"or": [("machine_one", None), ("machine_two", None)]}}

        self.machine.add_event(
            TestStateEvent,
            {None: "state_any_in_one", "state_any_in_one": "state_any_in_one"},
        )

        one = EventOne()
        two = EventTwo()

        r = self.machine.execute(one)
        with pytest.raises(KeyError):
            isinstance(r[(self.machine, "state_any_in_one")], TestStateEvent)
        assert self.nested_machine_one.state == "state_one"
        assert self.nested_machine_two.state is None
        assert self.machine.state is None

        r = self.machine.execute(two)
        assert isinstance(r[(self.machine, "state_any_in_one")], TestStateEvent)
        assert self.nested_machine_one.state == "state_one"
        assert self.nested_machine_two.state == "state_two"
        assert self.machine.state == "state_any_in_one"

        r = self.machine.execute(one)
        assert isinstance(r[(self.machine, "state_any_in_one")], TestStateEvent)
        assert self.nested_machine_one.state == "state_two"
        assert self.nested_machine_two.state == "state_two"
        assert self.machine.state == "state_any_in_one"

        r = self.machine.execute(two)
        assert isinstance(r[(self.machine, "state_any_in_one")], TestStateEvent)
        assert self.nested_machine_one.state == "state_one"
        assert self.nested_machine_two.state == "state_one"
        assert self.machine.state == "state_any_in_one"

    def test_back_propagation_and_state_event(self):
        class TestStateEvent(StateEvent):
            states = {
                "and": [("machine_one", "state_one"), ("machine_two", "state_one")]
            }

        self.machine.add_event(
            TestStateEvent,
            {None: "state_any_in_one", "state_any_in_one": "state_any_in_one"},
        )

        one = EventOne()
        two = EventTwo()

        r = self.nested_machine_one.execute(one)
        assert r == {(self.nested_machine_one, "state_one"): one}
        assert self.machine.state is None
        assert self.nested_machine_one.state == "state_one"
        assert self.nested_machine_two.state is None

        r = self.nested_machine_two.execute(two)
        assert r == {(self.nested_machine_two, "state_two"): two}
        assert self.machine.state is None
        assert self.nested_machine_one.state == "state_one"
        assert self.nested_machine_two.state == "state_two"

        r = self.nested_machine_two.execute(two)
        assert r == {(self.nested_machine_two, "state_one"): two}
        assert self.machine.state == "state_any_in_one"
        assert self.nested_machine_one.state == "state_one"
        assert self.nested_machine_two.state == "state_one"

        r = self.nested_machine_two.execute(two)
        assert r is None
        assert self.machine.state == "state_any_in_one"
        assert self.nested_machine_one.state == "state_one"
        assert self.nested_machine_two.state == "state_one"

        r = self.nested_machine_two.execute(one)
        assert r == {(self.nested_machine_two, "state_two"): one}
        assert self.machine.state == "state_any_in_one"
        assert self.nested_machine_one.state == "state_one"
        assert self.nested_machine_two.state == "state_two"
