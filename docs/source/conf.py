from pallets_sphinx_themes import ProjectLink

# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))


# -- Project information -----------------------------------------------------

project = 'State Engine'
copyright = '2021, Yu.A.Lygin'
author = 'Yu.A.Lygin'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    # 'rst2pdf.pdfbuilder',
    'sphinx.ext.autodoc',
    'sphinx.ext.intersphinx',
    'pallets_sphinx_themes',
    'sphinx_issues',
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []

# The name of the Pygments (syntax highlighting) style to use.
pygments_style = 'sphinx'


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'flask'
html_context = {
    'project_links': [
        ProjectLink('PyPI Releases', 'https://pypi.org/project/State-Engine/'),
        ProjectLink('Source Code', 'https://gitlab.com/yuriylygin/state-engine/'),
        ProjectLink(
            'Issue Tracker', 'https://gitlab.com/yuriylygin/state-engine/-/issues',
        ),
        ProjectLink('PDF', 'pdf/State-Engine.pdf'),
    ]
}
html_sidebars = {
    'index': ['project.html', 'localtoc.html', 'searchbox.html'],
    '**': ['localtoc.html', 'relations.html', 'searchbox.html'],
}
# Add any paths that contain custom static files (such as style sheets) here, 'project.html'
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# -- Options for PDF output -------------------------------------------------

master_doc = 'index'
rinoh_documents = [
    {
    'doc': master_doc,                # top-level file (index.rst)
    'target': 'State-Engine',         # output (target.pdf)
    'title': project,                 # document title
    'author': author,                 # document author
    # 'logo': '{!s}'.format(project),
    'template': 'article',
    },
]
rinoh_targets = ['State-Engine']
