API
===

.. module:: state_engine

Machines
--------

.. autoclass:: Moore
   :members:

Events
------

.. autoclass:: Event
   :members:
.. autoclass:: EpsilonEvent
   :members:
.. autoclass:: StateEvent
   :members:

Exceptions
----------

.. autoexception:: TransitionsImplementationError
   :members:
.. autoexception:: StateEventImplementationError
   :members: