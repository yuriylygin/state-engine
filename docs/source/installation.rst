.. _installation:

Installation
============

Install State-Engine with ``pip``:

.. code-block:: console

    pip install State-Engine


The development version can be downloaded from
`GitLab <https://gitlab.com/yuriylygin/state-engine>`_.

.. code-block:: console

    git clone https://gitlab.com/yuriylygin/state-engine
    cd state-engine
    pip install -e .[dev]


State-Engine requires Python version 3.6, 3.7, 3.8 or 3.9.

To build documentation run:

.. code-block:: console

    pip install -e .[docs]
    sphinx-build -b html docs/source/ docs/build/html
    sphinx-build -b rinoh docs/source/ docs/build/html/pdf

.. note::
   If you want to build documentation you should use Python of versions 3.7, 3.8 or 3.9 and not the version 3.6.