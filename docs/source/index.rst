   
State Engine
============

.. note::

   This project is under active development.


Documentation
-------------

This part of the documentation will show you how to get started in using
State-Engine.

.. toctree::
    :maxdepth: 2

    usage
    installation
    examples


API Reference
-------------

If you are looking for information on a specific function, class or
method, this part of the documentation is for you.

.. toctree::
   :maxdepth: 2

   api

Additional Notes
----------------
..
.. toctree::
   :maxdepth: 2

   contributing
   changelog
..

References
----------

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
