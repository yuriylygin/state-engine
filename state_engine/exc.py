class TransitionsImplementationError(Exception):
    pass


class StateEventImplementationError(Exception):
    pass
