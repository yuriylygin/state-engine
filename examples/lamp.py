import copy
from state_engine import Moore, Event, StateEvent


class Lamp(Moore):
    """
    Lamp can be switched on and off
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__intensity = None

    def trace(self):
        print(
            '"{}" is in state "{}" with intensity {}'.format(
                self.name, self.state, self.intensity
            )
        )

    @property
    def intensity(self):
        return self.__intensity

    def on(self, event):
        self.__intensity = event.intensity
        self.trace()
        return self.intensity

    def off(self, event):
        self.__intensity = 0
        self.trace()
        return self.intensity

    def fault(self, event):
        self.off(event)
        return self.intensity


class OnEvent(Event):
    def __init__(self, intensity=10):
        super().__init__()
        self.intensity = intensity


class OffEvent(Event):
    pass


class FaultEvent(Event):
    pass


class Lamps(Moore):
    def trace(self):
        print('"{}" is in state "{}"'.format(self.name, self.state))

    def all_working(self, event):
        self.trace()

    def some_fault(self, event):
        self.trace()

    def all_fault(self, event):
        self.trace()


class CorrectEvent(StateEvent):

    states = {
        "and": [
            {
                "not": {
                    "or": [
                        ("lamp_a", "fault"),
                        ("lamp_b", "fault"),
                    ]
                }
            },
            {
                "not": {
                    "or": [
                        ("lamp_a", None),
                        ("lamp_b", None),
                    ]
                }
            },
        ]
    }


class AccidentEvent(StateEvent):

    states = {
        "or": [
            ("lamp_a", "fault"),
            ("lamp_b", "fault"),
        ]
    }


class AlarmEvent(StateEvent):

    states = {
        "and": [
            ("lamp_a", "fault"),
            ("lamp_b", "fault"),
        ]
    }


lamp_a = (
    Lamp("lamp_a")
    .add_event(OnEvent, {None: "on", "off": "on", "on": "on"})
    .add_event(OffEvent, {None: "off", "on": "off"})
    .add_event(FaultEvent, {None: "fault", "on": "fault", "off": "fault"})
)

lamp_b = copy.deepcopy(lamp_a)
lamp_b.name = "lamp_b"

lamps = (
    Lamps("lamps")
    .add_machine(lamp_a)
    .add_machine(lamp_b)
    .add_event(
        CorrectEvent,
        {None: "all_working", "some_fault": "all_working", "all_fault": "all_working"},
    )
    .add_event(
        AccidentEvent,
        {None: "some_fault", "all_working": "some_fault", "all_fault": "some_fault"},
    )
    .add_event(
        AlarmEvent,
        {None: "all_fault", "all_working": "all_fault", "some_fault": "all_fault"},
    )
)


if __name__ == "__main__":

    print("Step 1")
    lamp_a.execute(OnEvent(10))
    lamp_b.execute(OffEvent())

    print("Step 2")
    lamp_a.execute(OnEvent(20))
    lamp_b.execute(OnEvent(10))

    print("Step 3")
    lamp_a.execute(OffEvent())
    lamp_b.execute(FaultEvent())

    print("Step 4")
    lamp_a.execute(OnEvent(50))
    lamp_b.execute(OffEvent())

    print("Step 5")
    lamp_a.execute(FaultEvent())
    lamp_b.execute(OnEvent())

    print("Step 6")
    lamp_a.execute(OnEvent())
    lamp_b.execute(OffEvent())
