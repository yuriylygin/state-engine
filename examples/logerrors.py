from pprint import pprint
from typing import List, Dict
import datetime as dt
from state_engine import Moore, Event, EpsilonEvent, StateEvent


class ErrorsLogger(Moore):

    errors: List = list()

    def ok(self, event):
        if len(self.errors):
            self.errors[-1]['end'] = dt.datetime.now().isoformat()

    def first_fault(self, event):
        self.errors.append({
            'start': dt.datetime.now().isoformat(),
            'ticks': dict(),
        })

    def fault(self, event):
        self.errors[-1]['ticks'].update({
            dt.datetime.now().isoformat(): event.message if event.message else None,
        })


class Ok(Event):
    pass


class Fault(Event):
    def __init__(self, massage=None):
        super().__init__()
        self.message = massage


errors_logger = ErrorsLogger('Template') \
    .add_event(Ok, {None: 'ok', 'fault': 'ok'}) \
    .add_event(EpsilonEvent, {'first_fault': 'fault'}) \
    .add_event(Fault, {None: 'first_fault', 'fault': 'fault', 'ok': 'first_fault'})


if __name__ == '__main__':
    import requests
    for slug in ['', '1', '2', '3', '', '1', '2']: 
        try:
            res = requests.get(f'https://google.com/{slug}')
            res.raise_for_status()
            errors_logger.execute(Ok())
        except requests.exceptions.HTTPError as e:
            errors_logger.execute(Fault(e))

    pprint(errors_logger.errors)